package geotraking.geotraking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.Toast;

public class MyLocationListener implements LocationListener{
		MainActivity Main;

		public MainActivity getMain() {
			return Main;
		}

		public void setMain(MainActivity Main) {
			this.Main = Main;
		}

		@Override
		public void onLocationChanged(Location loc) {
			// Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
			// debido a la deteccion de un cambio de ubicacion
			loc.getLatitude();
			loc.getLongitude();
			Constantes.ubicacion= "Lat = "
					+ loc.getLatitude() + "\n Long = " + loc.getLongitude();
			Toast.makeText(Main, Constantes.ubicacion, Toast.LENGTH_LONG).show();
			new Thread(new Runnable() {
				public void run() {
					//PujarUbicacio();
				}
			}).start();
			Constantes.mlocManager.removeUpdates(Constantes.mlocListener);
			setLocation(loc);
		}

		/*protected void PujarUbicacio() {
			try {
				 
		          HttpClient httpclient = new DefaultHttpClient();
		          HttpPost httppost = new HttpPost(MainActivity.urlUbicacion);
		          List<NameValuePair> send = new ArrayList<NameValuePair>();
		          send.add(new BasicNameValuePair("empresa",MainActivity.arrayNotis.get(pos).getEmpresa()));
		          send.add(new BasicNameValuePair("ubicacio",ubicacion));
		          httppost.setEntity(new UrlEncodedFormEntity(send));
		 
		          HttpResponse resp = httpclient.execute(httppost);
		          HttpEntity ent = resp.getEntity();
		          
		      }
		      catch(Exception e){ 
		    	  
		      }
			
		}
		 */
		@Override
		public void onProviderDisabled(String provider) {
			// Este metodo se ejecuta cuando el GPS es desactivado
			//messageTextView.setText("GPS Desactivado");
			Toast.makeText(Main, "GPS Desactivado", Toast.LENGTH_LONG).show();
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// Este metodo se ejecuta cuando el GPS es activado
		//	messageTextView.setText("GPS Activado");
			Toast.makeText(Main, "GPS Activado", Toast.LENGTH_LONG).show();
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// Este metodo se ejecuta cada vez que se detecta un cambio en el
			// status del proveedor de localizaci�n (GPS)
			// Los diferentes Status son:
			// OUT_OF_SERVICE -> Si el proveedor esta fuera de servicio
			// TEMPORARILY_UNAVAILABLE -> Temp�ralmente no disponible pero se
			// espera que este disponible en breve
			// AVAILABLE -> Disponible
		}

		
		public void setLocation(Location loc) {
			//Obtener la direccion de la calle a partir de la latitud y la longitud 
			if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
				try {
					Geocoder geocoder = new Geocoder(Main, Locale.getDefault());
					List<Address> list = geocoder.getFromLocation(
							loc.getLatitude(), loc.getLongitude(), 1);
					if (!list.isEmpty()) {
						Address address = list.get(0);
						Toast.makeText(Main, "Mi direccion es:\n"+address.getAddressLine(0), Toast.LENGTH_LONG).show();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			Constantes.mlocManager.removeUpdates(Constantes.mlocListener);
		}
}
