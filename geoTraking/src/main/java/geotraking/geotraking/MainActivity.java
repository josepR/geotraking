package geotraking.geotraking;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.drm.DrmStore.ConstraintsColumns;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends Activity {
	 static Properties mailServerProperties;
	 static Session getMailSession;
	 static MimeMessage generateMailMessage;
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		
		//TODO comprovar si hay ajustes, sino iniciar ajustes
		//Al iniciar lanza el escaner
		
		
		//comprovar gps activo
		Constantes.mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Constantes.mlocListener = new MyLocationListener();
		Constantes.mlocListener.setMain(this);
		Constantes.mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				Constantes.mlocListener);
		
		
		Intent intent = new Intent(
				"com.google.zxing.client.android.SCAN");
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
		startActivityForResult(intent, Constantes.QR);
	


	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

			if (resultCode == RESULT_OK) {
				switch(requestCode){
				case 0:
					//guardamos codigo qr
					Constantes.Codigoqr=intent.getStringExtra("SCAN_RESULT");
					System.out.println("Codigo qr resultado:"+Constantes.Codigoqr);
					//lanzamos eleccion de imagen
					LanzarCamara();
					break;
				case 1:
					//Elegimos foto de la galeria
					 Uri UriGal = intent.getData();  
	    	         Constantes.fotoencode=Constantes.getPath(UriGal,this);
	    	         System.out.println(Constantes.fotoencode);
	    	         EnviarMail run = new EnviarMail();
	    	 		run.execute();
					break;
				case 2:
					//Elegimos camera
					Uri UriCam = intent.getData();     
		            Constantes.fotoencode=Constantes.getPath(UriCam,this);
		            System.out.println(Constantes.fotoencode);
		            EnviarMail run1 = new EnviarMail();
		    		run1.execute();
					break;
				}
				
				
				
				/*
				tvStatus.setText(intent.getStringExtra("SCAN_RESULT_FORMAT"));
				tvResult.setText(intent.getStringExtra("SCAN_RESULT"));
				*/
			} else if (resultCode == RESULT_CANCELED) {
				//tvStatus.setText("Press a button to start a scan.");
				//tvResult.setText("Scan cancelled.");
				Toast.makeText(this, "Heu cancelat", Toast.LENGTH_SHORT).show();
			}
	}

	void LanzarCamara(){

    	Intent pictureActionIntent = new Intent();
    	pictureActionIntent.setAction(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
    	startActivityForResult(pictureActionIntent, Constantes.camera);
                    	
	}
	
	void EnviarEmail(){
		 try {   
			 GMailSender sender= new GMailSender("sirpeta2@gmail.com", "rosello0");
             sender.sendMail(Constantes.Codigoqr,   
                     "Foto codificada:" ,   
                     "sirpeta2@gmail.com",    
                     "rosello_7@hotmail.com,sirpeta2@gmail.com",
                     Constantes.fotoencode);   
         } catch (Exception e) {   
             Log.e("SendMail", e.getMessage(), e);   
             System.out.println("Error al EnviarEmail");
         } 

	}
	
	
	//Asynctask enviar email
	 class EnviarMail extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
				EnviarEmail();
			return null;
		}
		 
	 }
	
	
}

