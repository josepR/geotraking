package geotraking.geotraking;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Toast;

public class Constantes {
	
	//guarda el codigo al escanear
	static String Codigoqr;
	//foto encode64
	static String fotoencode;
	//localizacion cuando se envia el mail
	static String localizacion;
	//fecha cuando se envia el mail
	static String fecha;
	
	//Keys para onResult
	static int QR=0;
	static int gallery=1;
	static int camera=2;
		
	static String ubicacion;
	
	 static LocationManager mlocManager ;
	 static MyLocationListener mlocListener; 
	
	public static String getPathEncoded(Uri uri,Activity a) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = a.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path=cursor.getString(column_index);
        byte[] encodedBytes = Base64.encode(path.getBytes(),0);
        System.out.println("encodedBytes " + new String(encodedBytes));
        return encodedBytes.toString();
    }
	public static String getPath(Uri uri,Activity a) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = a.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	
}
